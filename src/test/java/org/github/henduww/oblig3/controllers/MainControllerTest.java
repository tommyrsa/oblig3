package org.github.henduww.oblig3.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import org.github.henduww.oblig3.App;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.testfx.api.FxAssert.verifyThat;

@ExtendWith(ApplicationExtension.class)
public class MainControllerTest {

    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("views/MainView.fxml"));
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);

        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }

    @Test
    public void clickDealHand_DealsCARDS_PER_HANDCards(FxRobot fxRobot) {
        this.clickDealHand(fxRobot);

        FlowPane cardPane = fxRobot.lookup("#cardPane").query();

        assertEquals(App.CARDS_PER_HAND, cardPane.getChildren().size());
    }

    // Helpers

    private void clickDealHand(FxRobot fxRobot) {
        final String dealHandBtnID = "#dealHandBtn";
        fxRobot.clickOn(dealHandBtnID);
    }

    private void clickCheckHand(FxRobot fxRobot) {
        final String checkHandBtnID = "#checkHandBtn";
        fxRobot.clickOn(checkHandBtnID);
    }

}
