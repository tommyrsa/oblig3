package org.github.henduww.oblig3.models;

import org.github.henduww.oblig3.models.CardSuit;
import org.github.henduww.oblig3.models.PlayingCard;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Playing card representation and generation")
public class PlayingCardTest {

    @Nested
    @DisplayName("Constructor tests")
    class ConstructorTest {
        @Test
        @DisplayName("Constructor throws NullPointException")
        public void constructorThrowsNullPointerExceptionTest() {
            assertThrows(NullPointerException.class, () -> new PlayingCard(null, 13));
        }

        @Test
        @DisplayName("Constructor throws IllegalArgumentException")
        public void constructorThrowsIllegalArgumentExceptionTest() {
            assertThrows(IllegalArgumentException.class, () -> new PlayingCard(CardSuit.DIAMONDS, 0));
            assertThrows(IllegalArgumentException.class, () -> new PlayingCard(CardSuit.DIAMONDS, -50));
            assertThrows(IllegalArgumentException.class, () -> new PlayingCard(CardSuit.DIAMONDS, 14));
            assertThrows(IllegalArgumentException.class, () -> new PlayingCard(CardSuit.DIAMONDS, 500));
        }

        @Test
        @DisplayName("Valid constructor call throws nothings")
        public void validConstructorCallThrowsNothingTest() {
            assertDoesNotThrow(() -> new PlayingCard(CardSuit.DIAMONDS, 13));
        }
    }

    @Nested
    @DisplayName("Getter tests")
    class GetterTest {
        private final CardSuit suit = CardSuit.CLUBS;
        private final int face = 6;

        @Test
        @DisplayName("Suit")
        public void getSuitTest() {
            PlayingCard playingCard = new PlayingCard(this.suit, this.face);

            assertEquals(suit, playingCard.getSuit());
        }

        @Test
        @DisplayName("Face")
        public void getFaceTest() {
            PlayingCard playingCard = new PlayingCard(this.suit, this.face);

            assertEquals(face, playingCard.getFace());
        }

        @Test
        @DisplayName("As String")
        public void getAsStringTest() {
            PlayingCard playingCard = new PlayingCard(this.suit, this.face);

            assertEquals("C6", playingCard.getAsString());
        }
    }

    @Nested
    @DisplayName("Equals tests")
    class EqualsTest {
        @Test
        @DisplayName("Equal objects return true")
        public void equalObjectsReturnTrueTest() {
            final CardSuit suit = CardSuit.SPADES;
            final int face = 9;

            PlayingCard card1 = new PlayingCard(suit, face);
            PlayingCard card2 = new PlayingCard(suit, face);

            assertEquals(card2, card1);
        }

        @Test
        @DisplayName("Unequal objects return false")
        public void unequalObjectsReturnFalseTest() {
            final CardSuit suit = CardSuit.SPADES;

            PlayingCard card1 = new PlayingCard(suit, 3);
            PlayingCard card2 = new PlayingCard(suit, 8);

            assertNotEquals(card2, card1);
        }
    }
}
