package org.github.henduww.oblig3.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeckOfCardsTest {
    @Test
    @DisplayName("A new deck of cards has 52 cards")
    public void newDeckHas52CardsTest() {
        DeckOfCards deck = new DeckOfCards();

        assertEquals(52, deck.cardsLeft());
    }

    @Nested
    @DisplayName("Hand dealing tests")
    class DealHandTest {
        @Test
        @DisplayName("Dealt cards are removed from deck")
        public void dealtCardsDisappear() {
            DeckOfCards deck = new DeckOfCards();

            int cardsToRemove = 15;
            int expectedRemainder = deck.cardsLeft() - cardsToRemove;

            deck.dealHand(cardsToRemove);

            assertEquals(expectedRemainder, deck.cardsLeft());
        }
        @Nested
        @DisplayName("Invalid inputs are handled correctly")
        class InvalidInputsTest {
            @Test
            @DisplayName("Less than 1 card cannot be dealt")
            public void tooFewCardsToDealTest() {
                DeckOfCards deck = new DeckOfCards();

                assertThrows(IllegalArgumentException.class, () -> deck.dealHand(0));
            }

            @Test
            @DisplayName("More than 52 cards cannot be dealt")
            public void tooManyCardsToDealTest() {
                DeckOfCards deck = new DeckOfCards();

                assertThrows(IllegalArgumentException.class, () -> deck.dealHand(55));
            }

            @Test
            @DisplayName("Cannot deal more cards than are available in the deck")
            public void tooFewCardsInDeckTest() {
                DeckOfCards deck = new DeckOfCards();

                // First simulate removing some cards
                deck.dealHand(20);

                // We know there are 32 cards left, so trying to retrieve more than 32 cards should fail
                assertThrows(IndexOutOfBoundsException.class, () -> deck.dealHand(40));
            }
        }

        @Nested
        @DisplayName("Valid inputs are handled correctly by")
        class ValidInputsTest {
            @Test
            @DisplayName("Exactly 1 card may be dealt")
            public void exactly1DealtTest() {
                DeckOfCards deck = new DeckOfCards();

                HandOfCards hand = deck.dealHand(1);
                assertEquals(hand.amountOfCards(), 1);
            }

            @Test
            @DisplayName("Exactly 34 cards may be dealt")
            public void exactly34DealtTest() {
                DeckOfCards deck = new DeckOfCards();

                HandOfCards hand = deck.dealHand(34);
                assertEquals(hand.amountOfCards(), 34);
            }

            @Test
            @DisplayName("Exactly 52 cards may be dealt")
            public void exactly52DealtTest() {
                DeckOfCards deck = new DeckOfCards();

                HandOfCards hand = deck.dealHand(52);
                assertEquals(hand.amountOfCards(), 52);
            }
        }
    }
}
