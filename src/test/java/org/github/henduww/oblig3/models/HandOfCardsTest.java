package org.github.henduww.oblig3.models;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {
    @Nested
    @DisplayName("Constructor tests")
    class ConstructorTest {
        @Test
        @DisplayName("Null argument throws NullPointerException")
        public void nullArgThrowsNullPointerExceptionTest() {
            assertThrows(NullPointerException.class, () -> new HandOfCards(null));
        }

        @Test
        @DisplayName("Valid list arg does not throw")
        public void validListArgDoesNotThrowTest() {
            assertDoesNotThrow(() -> new HandOfCards(new ArrayList<>()));
        }
    }

    @Nested
    @DisplayName("addCard tests")
    class AddCardTest {
        @Test
        @DisplayName("Null argument throws NullPointerException")
        public void nullArgThrowsNullPointerExceptionTest() {
            assertThrows(NullPointerException.class, () -> new HandOfCards().addCard(null));
        }

        @Test
        @DisplayName("Valid card arg does not throw")
        public void validCardArgDoesNotThrowTest() {
            assertDoesNotThrow(() -> new HandOfCards().addCard(new PlayingCard(CardSuit.DIAMONDS, 1)));
        }

        @Test
        @DisplayName("Card is added to hand when it's not already in hand")
        public void addCardReturnsTrueTest() {
            assertTrue(new HandOfCards().addCard(new PlayingCard(CardSuit.HEARTS, 11)));
        }

        @Test
        @DisplayName("Card is not added to hand when it's already in hand")
        public void addCardReturnsFalseTest() {
            HandOfCards hand = new HandOfCards();
            PlayingCard card = new PlayingCard(CardSuit.SPADES, 3);

            hand.addCard(card);

            assertFalse(hand.addCard(card));
        }
    }

    @Nested
    @DisplayName("removeCard tests")
    class RemoveCardTest {
        @Test
        @DisplayName("Null argument throws NullPointerException")
        public void nullArgThrowsNullPointerExceptionTest() {
            assertThrows(NullPointerException.class, () -> new HandOfCards().removeCard(null));
        }

        @Test
        @DisplayName("Valid card arg does not throw")
        public void validCardArgDoesNotThrowTest() {
            assertDoesNotThrow(() -> new HandOfCards().removeCard(new PlayingCard(CardSuit.DIAMONDS, 1)));
        }

        @Test
        @DisplayName("Card is removed from hand if it exists there")
        public void removeCardReturnsTrueTest() {
            HandOfCards hand = new HandOfCards();
            PlayingCard card = new PlayingCard(CardSuit.SPADES, 3);

            hand.addCard(card);

            assertTrue(hand.removeCard(card));
        }

        @Test
        @DisplayName("Card is not removed from hand if it does not exist there")
        public void removeCardReturnsFalseTest() {
            assertFalse(new HandOfCards().removeCard(new PlayingCard(CardSuit.HEARTS, 2)));
        }
    }

    @Test
    @DisplayName("Amount of cards display correctly")
    public void amountOfCardsDisplayCorrectlyTest() {
        final ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard(CardSuit.HEARTS, 1));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 2));
        cards.add(new PlayingCard(CardSuit.SPADES, 7));

        HandOfCards hand = new HandOfCards(cards);

        assertEquals(cards.size(), hand.amountOfCards());
    }

    @Test
    @DisplayName("getPlayingCards() returns the correct list")
    public void getPLayingCardsReturnsCorrectListTest() {
        final ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard(CardSuit.HEARTS, 1));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 2));
        cards.add(new PlayingCard(CardSuit.SPADES, 7));

        HandOfCards hand = new HandOfCards(cards);

        assertEquals(cards, hand.getPlayingCards());
    }

    @Test
    @DisplayName("sumOfFaces() returns the correct sum")
    public void sumOfFacesReturnsCorrectSum() {
        int face1, face2, face3;

        final ArrayList<PlayingCard> cards = new ArrayList<>();
        cards.add(new PlayingCard(CardSuit.HEARTS, face1 = 1));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, face2 = 2));
        cards.add(new PlayingCard(CardSuit.SPADES, face3 = 7));

        HandOfCards hand = new HandOfCards(cards);

        int expectedSum = face1 + face2 + face3;

        assertEquals(expectedSum, hand.sumOfFaces());
    }

    @Test
    @DisplayName("getCardsBySuit() only returns cards of the specified suit")
    public void getCardsBySuitReturnsOnlyCardsOfSpecifiedSuit() {
        final ArrayList<PlayingCard> diamondCards = new ArrayList<>();
        diamondCards.add(new PlayingCard(CardSuit.DIAMONDS, 1));
        diamondCards.add(new PlayingCard(CardSuit.DIAMONDS, 4));
        diamondCards.add(new PlayingCard(CardSuit.DIAMONDS, 11));

        final ArrayList<PlayingCard> cards = new ArrayList<>(diamondCards);
        cards.add(new PlayingCard(CardSuit.HEARTS, 1));
        cards.add(new PlayingCard(CardSuit.CLUBS, 2));
        cards.add(new PlayingCard(CardSuit.SPADES, 7));

        HandOfCards hand = new HandOfCards(cards);

        assertEquals(diamondCards, hand.getCardsBySuit(CardSuit.DIAMONDS));
    }

    @Test
    @DisplayName("hasCard() returns true if specific card is in hand")
    public void hasCardReturnsTrueIfCardIsInHand() {
        HandOfCards hand = new HandOfCards();
        PlayingCard card = new PlayingCard(CardSuit.SPADES, 3);

        hand.addCard(card);

        assertTrue(hand.hasCard(card));
    }

    @Test
    @DisplayName("hasCard() returns false if specific card is not in hand")
    public void hasCardReturnsFalseIfCardIsNotInHand() {
        HandOfCards hand = new HandOfCards();
        PlayingCard card1 = new PlayingCard(CardSuit.SPADES, 3);
        PlayingCard card2 = new PlayingCard(CardSuit.CLUBS, 8);

        hand.addCard(card1);

        assertFalse(hand.hasCard(card2));
    }

    @Test
    @DisplayName("hasFiveFlush() returns true if there are 5 cards of the same suit in hand")
    public void hasFiveFlushReturnsTrueIf5OfSuitIsInHand() {
        final ArrayList<PlayingCard> cards = new ArrayList<>();

        cards.add(new PlayingCard(CardSuit.DIAMONDS, 1));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 4));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 11));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 13));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 5));

        cards.add(new PlayingCard(CardSuit.HEARTS, 1));
        cards.add(new PlayingCard(CardSuit.CLUBS, 2));
        cards.add(new PlayingCard(CardSuit.SPADES, 7));

        HandOfCards hand = new HandOfCards(cards);

        assertTrue(hand.hasFiveFlush());
    }

    @Test
    @DisplayName("hasFiveFlush() returns false if there are not 5 cards of the same suit in hand")
    public void hasFiveFlushReturnsFalseIf5OfSuitIsNotInHand() {
        final ArrayList<PlayingCard> cards = new ArrayList<>();

        cards.add(new PlayingCard(CardSuit.DIAMONDS, 1));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 4));
        cards.add(new PlayingCard(CardSuit.DIAMONDS, 11));

        cards.add(new PlayingCard(CardSuit.HEARTS, 1));
        cards.add(new PlayingCard(CardSuit.CLUBS, 2));
        cards.add(new PlayingCard(CardSuit.SPADES, 7));

        HandOfCards hand = new HandOfCards(cards);

        assertFalse(hand.hasFiveFlush());
    }
}
