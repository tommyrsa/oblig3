package org.github.henduww.oblig3.controllers;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.TextAlignment;
import org.github.henduww.oblig3.App;
import org.github.henduww.oblig3.models.CardSuit;
import org.github.henduww.oblig3.models.DeckOfCards;
import org.github.henduww.oblig3.models.HandOfCards;
import org.github.henduww.oblig3.models.PlayingCard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainController {
    @FXML
    private FlowPane cardPane;

    @FXML
    private Label sumOfFaces;

    @FXML
    private Label flushCheck;

    @FXML
    private Label cardsOfHearts;

    @FXML
    private Label queenOfSpadesCheck;

    // Game field variables
    private DeckOfCards deckOfCards;
    private HandOfCards playerHand;

    /**
     * JavaFX initialization of the scene.
     */
    @FXML
    private void initialize() {
        this.deckOfCards = new DeckOfCards();
    }

    /**
     * Listener for Deal Hand button.
     * @param event Event context.
     */
    @FXML
    private void onDealHand(ActionEvent event) {
        try {
            this.playerHand = this.deckOfCards.dealHand(App.CARDS_PER_HAND);
        } catch (IndexOutOfBoundsException e) {
            if (this.restartGameDialog()) {
                this.restartGame();
                return;
            }
        }

        this.cardPane.getChildren().setAll(this.getCardsAsLabels());
    }

    /**
     * Listener for Check Hand button.
     * @param event Event context.
     */
    @FXML
    private void onCheckHand(ActionEvent event) {
        if (this.playerHand != null) {
            this.updateHandInfo();
        }
    }

    /**
     * Helper that updates info about the hand.
     * Called by {@code onCheckHand}.
     */
    private void updateHandInfo() {
        this.sumOfFaces.setText(Integer.toString(this.playerHand.sumOfFaces()));

        String hasFlushString = this.playerHand.hasFiveFlush()
                            ? "Yes"
                            : "No";
        this.flushCheck.setText(hasFlushString);

        ArrayList<PlayingCard> heartsOnHand = this.playerHand.getCardsBySuit(CardSuit.HEARTS);
        String heartsOnHandString = heartsOnHand.isEmpty()
                                    ? "None"
                                    : heartsOnHand.stream().map(PlayingCard::getAsString).collect(Collectors.joining(", "));
        this.cardsOfHearts.setText(heartsOnHandString);

        String hasQueenOfSpadesString = this.playerHand.hasCard(new PlayingCard(CardSuit.SPADES, 12))
                                    ? "Yes"
                                    : "No";
        this.queenOfSpadesCheck.setText(hasQueenOfSpadesString);
    }

    /**
     * @return {@code playerHand} as JavaFX labels.
     */
    private List<Label> getCardsAsLabels() {
        final ArrayList<Label> cardsAsLabels = new ArrayList<>();

        this.playerHand
                .getPlayingCards()
                .forEach((card) -> cardsAsLabels.add(generateLabelFromCard(card)));

        return cardsAsLabels;
    }

    /**
     * Restarts the game by clearing the flowpane containing cards and reinitializing the deck of cards.
     */
    private void restartGame() {
        this.cardPane.getChildren().clear();
        this.deckOfCards = new DeckOfCards();
    }

    /**
     * Dialog window to ask the user whether or not the user wants to restart.
     * @return {@code false} if the user does not want to restart the game. {@code true} if the user wants to restart the game.
     */
    private boolean restartGameDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Not enough cards left");
        alert.setHeaderText("There aren't enough cards left in the deck.\nRestarting game.");
        alert.setContentText("Clicking \"OK\" will restart the game and give you a new deck.");

        Optional<ButtonType> result = alert.showAndWait();
        return result.filter(buttonType -> buttonType == ButtonType.OK).isPresent();
    }

    /**
     * Generates a JavaFX label from a {@code PlayingCard} object.
     * @param card Card to generate a label from.
     * @return Generated label.
     */
    private Label generateLabelFromCard(PlayingCard card) {
        Label label = new Label(card.getAsString());

        label.setStyle("-fx-background-color: #bababa; -fx-padding: 5px 10px; -fx-background-radius: 10px; -fx-font-size: 12px; -fx-stroke: black; -fx-stroke-width: 2px; -fx-min-width: 100px; -fx-min-height: 100px;");

        return label;
    }

}
