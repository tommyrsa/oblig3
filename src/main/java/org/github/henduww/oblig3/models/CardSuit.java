package org.github.henduww.oblig3.models;

import java.util.List;

/**
 * Enumerated type that represents card suits.
 */
public enum CardSuit {
    SPADES {
        @Override
        public String toString() {
            return "S";
        }
    },
    HEARTS {
        @Override
        public String toString() {
            return "H";
        }
    },
    CLUBS {
        @Override
        public String toString() {
            return "C";
        }
    },
    DIAMONDS {
        @Override
        public String toString() {
            return "D";
        }
    }
}
