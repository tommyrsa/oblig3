package org.github.henduww.oblig3.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Represents a hand of cards held by a player of the game.
 */
public class HandOfCards {
    private final ArrayList<PlayingCard> playingCards;

    /**
     * Default constructor.
     * Initializes a hand of cards with no cards on hand.
     */
    public HandOfCards() {
        this.playingCards = new ArrayList<>();
    }

    /**
     * Parameterized constructor.
     * Initializes a hand of cards with {@code playingCards}.
     * @param playingCards The cards to have on hand.
     * @throws NullPointerException If {@code playingCards} is null.
     */
    public HandOfCards(ArrayList<PlayingCard> playingCards) throws NullPointerException {
        Objects.requireNonNull(playingCards);

        this.playingCards = playingCards;
    }

    /**
     * Adds a card to the hand.
     * @param card Card to add.
     * @return {@code false} if the card is already on hand. {@code true} if the card is not on hand and was added.
     * @throws NullPointerException If {@code card} is null.
     */
    public boolean addCard(PlayingCard card) throws NullPointerException {
        Objects.requireNonNull(card);

        // The way PlayingCard is defined, there is literally no way to get more than 52 cards.
        // Given that there are no duplicates.
        // 13 x 4 = 52
        if (this.playingCards.contains(card)) {
            return false;
        }

        this.playingCards.add(card);
        return true;
    }

    /**
     * @return Cards on hand.
     */
    public ArrayList<PlayingCard> getPlayingCards() {
        return this.playingCards;
    }

    /**
     * Removes a card from the hand.
     * @param card Card to remove.
     * @return {@code false} if the card is not on hand. {@code true} if the card is on hand and was removed.
     * @throws NullPointerException If {@code card} is null.
     */
    public boolean removeCard(PlayingCard card) throws NullPointerException {
        Objects.requireNonNull(card);

        if (!this.playingCards.contains(card)) {
            return false;
        }

        this.playingCards.remove(card);
        return true;
    }

    /**
     * @return Amount of cards on hand.
     */
    public int amountOfCards() {
        return this.playingCards.size();
    }

    /**
     * @return Sum of faces in the hand.
     */
    public int sumOfFaces() {
        return this.playingCards
                .stream()
                .mapToInt(PlayingCard::getFace)
                .sum();
    }

    /**
     * Gets cards on hand by their suit.
     * @param suit Suit to filter by.
     * @return Cards on hand with corresponding suit.
     */
    public ArrayList<PlayingCard> getCardsBySuit(CardSuit suit) {
        return (ArrayList<PlayingCard>) this.playingCards
                .stream()
                .filter((card) -> card.getSuit().equals(suit))
                .collect(Collectors.toList());
    }

    /**
     * Checks whether or not a specific card is on hand.
     * @param card Card to find.
     * @return {@code false} if the card is not hand. {@code true} if the card is on hand.
     */
    public boolean hasCard(PlayingCard card) {
        return this.playingCards
                .stream()
                .anyMatch((playingCard) -> playingCard.equals(card));
    }

    /**
     * @return Whether or not the hand has a five flush.
     */
    public boolean hasFiveFlush() {
        return Arrays.stream(CardSuit.values())
                .map(this::getCardsBySuit)
                .anyMatch((cardsBySuit) -> cardsBySuit.size() >= 5);
    }
}
