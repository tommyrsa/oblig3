package org.github.henduww.oblig3.models;

import java.util.Objects;

/**
 * Represents a playing card. A playing card has a number (face) between
 * 1 and 13, where 1 is called an Ace, 11 = Knight, 12 = Queen and 13 = King.
 * The card can also be one of 4 suits: Spade, Heart, Diamonds and Clubs.
 *
 * @author ntnu, Tommy René Sætre
 * @version 2021-04-09
 */
public class PlayingCard {

    // Modified by student to match enum
    private final CardSuit suit;
    private final int face; // a number between 1 and 13

    /**
     * Creates an instance of a PlayingCard with a given suit and face.
     *
     * @param suit The suit of the card, as a single character. 'S' for Spades,
     *             'H' for Heart, 'D' for Diamonds and 'C' for clubs.
     * @param face The face value of the card, an integer between 1 and 13, including bounds.
     * @throws NullPointerException If {@code suit} is null.
     * @throws IllegalArgumentException If {@code face} is not a value between 1 and 13, including bounds.
     */
    public PlayingCard(CardSuit suit, int face) throws NullPointerException, IllegalArgumentException {
        Objects.requireNonNull(suit, "Argument `suit` cannot be null.");

        if (face < 1 || face > 13) {
            throw new IllegalArgumentException("Argument `face` must be a value between 1 and 13.");
        }

        this.suit = suit;
        this.face = face;
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit.toString(), face);
    }

    /**
     * Returns the suit of the card, 'S' for Spades, 'H' for Heart, 'D' for Diamonds and 'C' for clubs
     *
     * @return the suit of the card
     */
    public CardSuit getSuit() {
        return suit;
    }

    /**
     * Returns the face of the card (value between 1 and 13).
     *
     * @return the face of the card
     */
    public int getFace() {
        return face;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (!(obj instanceof PlayingCard)) return false;
        PlayingCard playingCard = (PlayingCard) obj;

        return this.face == playingCard.getFace() && this.suit == playingCard.getSuit();

    }
}