package org.github.henduww.oblig3.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Represents a deck of 52 standard cards with 4 suits and 13 faces each.
 */
public class DeckOfCards {
    /**
     * A deck of cards has 4 suits
     */
    public static final int SUIT_AMT = 4;

    /**
     * A deck of cards has 13 faces
     */
    public static final int FACE_AMT = 13;

    private final ArrayList<PlayingCard> playingCards;

    /**
     * Default constructor.
     */
    public DeckOfCards() {
        this.playingCards = new ArrayList<>();

        initializeDeck();
    }

    /**
     * Deals a hand with {@code amt} random cards based on remaining cards in the deck.
     * @param amt Amount of random cards to deal.
     * @return {@code HandOfCards} with the {@code amt} random cards.
     * @throws IllegalArgumentException If {@code amt} ∉ [1, 52]
     * @throws IndexOutOfBoundsException If there arent {@code amt} cards left in the deck.
     */
    public HandOfCards dealHand(int amt) throws IllegalArgumentException, IndexOutOfBoundsException {
        if (amt < 1 || amt > 52) {
            throw new IllegalArgumentException("You can only have 1 to 52 cards dealt at a time.");
        }
        else if (this.cardsLeft() < amt) {
            throw new IndexOutOfBoundsException("There aren't that many remaining cards in the deck.");
        }

        HandOfCards hand = new HandOfCards();

        Random random = new Random();

        for (int i = 0; i < amt; i++) {
            PlayingCard randomCard = this.cardsLeft() != 1
                    ? this.playingCards.get(random.nextInt(this.cardsLeft() - 1))
                    : this.playingCards.get(0);

            hand.addCard(randomCard);
            this.playingCards.remove(randomCard);
        }

        return hand;
    }

    /**
     * @return Amount of cards left in the deck.
     */
    public int cardsLeft() {
        return this.playingCards.size();
    }

    /**
     * Creates one unique card object for each card in a real deck of cards.
     */
    private void initializeDeck() {
        // Loop through all defined suits
        for (int i = 0; i < SUIT_AMT; i++) {
            // Loop through all defined faces
            for (int j = 1; j <= FACE_AMT; j++) {
                // Add the unique card to the deck
                this.playingCards.add(new PlayingCard(CardSuit.values()[i], j));
            }
        }
    }
}
