module oblig3 {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.github.henduww.oblig3 to javafx.fxml;
    opens org.github.henduww.oblig3.controllers to javafx.fxml;
    opens org.github.henduww.oblig3.models to javafx.base;

    exports org.github.henduww.oblig3;
}